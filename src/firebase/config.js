import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyCoV-72jwVWFzdC3Pz1xtxCWhbeFlIT-ZQ",
  authDomain: "instagram-clone-ece0e.firebaseapp.com",
  databaseURL: "https://instagram-clone-ece0e.firebaseio.com",
  projectId: "instagram-clone-ece0e",
  storageBucket: "instagram-clone-ece0e.appspot.com",
  messagingSenderId: "131179202111",
  appId: "1:131179202111:web:af07d9dce59c0d1d205a8a",
};
// Initialize Firebase
// Initialize Firebase
class FirebaseClass {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.auth = firebase.auth();
    this.store = firebase.firestore();
    this.storage = firebase.storage();
  }

  // get All Post
  userGetAllPost = () => {
    return this.store
      .collection("posts")
      .get()
      .then(function (querySnapshot) {
        let posts = [];
        querySnapshot.forEach(function (doc) {
          // doc.data() is never undefined for query doc snapshots
          //    console.log(doc.id, " => ", doc.data());
          posts.push({ doc_id: doc.id, doc_data: doc.data() });
        });
        return posts;
      });
  };

  doLoginUser = ({ email, password }) => {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  };

  doRegisterUser = ({ username, email, password }) => {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  };
  doUploadPostUser = ({ file, uid }) => {
    let storageRef = firebase.storage().ref();

    let uploadTask = storageRef
      .child(`user_posts/${file.name}_${Math.random().toString()}`)
      .put(file);

    // Register three observers:
    // 1. 'state_changed' observer, called any time the state changes
    // 2. Error observer, called on failure
    // 3. Completion observer, called on successful completion
    uploadTask.on(
      "state_changed",
      function (snapshot) {
        // Observe state change events such as progress, pause, and resume
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload is " + progress + "% done");
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            console.log("Upload is paused");
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log("Upload is running");
            break;
        }
      },
      function (error) {
        // Handle unsuccessful uploads
      },
      function () {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          console.log("File available at", downloadURL);
        });
      }
    );

    console.log(file, uid);
  };
}

export default new FirebaseClass();
