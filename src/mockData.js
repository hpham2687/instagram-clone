export const mockData = [
  {
    userName: "Hung pham",
    userAvatar:
      "https://cdn2.vectorstock.com/i/1000x1000/38/31/male-face-avatar-logo-template-pictogram-vector-11333831.jpg",

    postImages: [
      "https://www.talkwalker.com/images/2020/blog-headers/image-analysis.png",
      "abc.png",
      "abc.png",
    ],
    postComments: [
      {
        from: "hung pham",
        text: "day la noi dung",
        replyComments: [
          { from: "hung pham", text: "day la noi dung" },
          { from: "hung pham", text: "day la noi dung" },
        ],
      },
    ],
  },
  {
    userName: "Hung pha2m",
    userAvatar:
      "https://cdn2.vectorstock.com/i/1000x1000/38/31/male-face-avatar-logo-template-pictogram-vector-11333831.jpg",

    postImages: [
      "https://www.talkwalker.com/images/2020/blog-headers/image-analysis.png",
      "abc.png",
      "abc.png",
    ],
    postComments: [
      {
        from: "hung pha2m",
        text: "day la noi dung",
        replyComments: [
          { from: "hung pham", text: "day la noi dung" },
          { from: "hung pham", text: "day la noi dung" },
        ],
      },
    ],
  },
];
