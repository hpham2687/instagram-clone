import React from "react";
import { useSelector, useDispatch } from "react-redux";
import ModalAuth from "../components/Modal/auth";
import { setModalAuthStatus } from "./../redux/actions/ui";
import firebaseCenter from "./../firebase/config";
import { useSnackbar } from "notistack";
const ModalContainer = (props) => {
  const modalState = useSelector((state) => state.ui.modal);
  const isShowModalAuth = modalState.isShowModalAuth;
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const dispatch = useDispatch();

  const handleOpen = () => {
    dispatch(setModalAuthStatus(true));
  };

  const handleClose = () => {
    dispatch(setModalAuthStatus(false));
  };

  const handleClickLogin = (formData) => {
    firebaseCenter
      .doLoginUser(formData)
      .then(() => {
        enqueueSnackbar("Login successfully");
        handleClose();
      })
      .catch(function (error) {
        // Handle Errors here.
        var errorMessage = error.message;
        enqueueSnackbar(errorMessage);
        // ...
      });
  };

  const handleClickSignUp = (formData) => {
    console.log(formData);
    firebaseCenter
      .doRegisterUser(formData)
      .then(() => {
        enqueueSnackbar("Sign up successfully");

        handleClose();
      })
      .catch(function (error) {
        // Handle Errors here.
        var errorMessage = error.message;
        enqueueSnackbar(errorMessage);

        // ...
      });
  };

  return (
    <>
      <ModalAuth
        open={isShowModalAuth}
        handleOpen={handleOpen}
        handleClose={handleClose}
        handleClickLogin={handleClickLogin}
        handleClickSignUp={handleClickSignUp}
      />
    </>
  );
};

export default ModalContainer;
