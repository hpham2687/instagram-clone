import React from "react";
import Post from "../components/Post/post";
import UploadPost from "../components/Post/uploadPost";
import firebaseCenter from "./../firebase/config";

import { useSelector } from "react-redux";

const HomeContainer = (props) => {
  const postState = useSelector((state) => state.post);
  const user = useSelector((state) => state.user);
  //console.log(user);
  const postData = postState.data;
  const isFetching = postState.isFetching;
  console.log(user);
  const handleUploadPostUser = (file) => {
    firebaseCenter.doUploadPostUser({ file, uid: user.uid });
  };
  return (
    <>
      <UploadPost handleUploadPostUser={handleUploadPostUser} />
      {isFetching
        ? "Loading..."
        : postData.map((post, index) => (
            <Post key={index} index={index} data={post} />
          ))}
    </>
  );
};

export default HomeContainer;
