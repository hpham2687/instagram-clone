import React from "react";
import "./post.scss";

/*
props.data = {
  userName: ,
  postImages: [
    "abc.png", "abc.png", "abc.png"
  ] ,

 postImages: [
    "abc.png", "abc.png", "abc.png"
  ] ,
  postComments: [
    {
      from: "hung pham",
      text: "day la noi dung",
      replyComments: [
              { from: "hung pham",
                 text: "day la noi dung"
              }, 
          { from: "hung pham",
            text: "day la noi dung"
          }
      ]
    }
  ]
}

*/
function Post({ data }) {
  const { userAvatar, userName, postComments, postImages } = data.doc_data;
  return (
    <div className="post">
      <div class="post__header">
        <div class="post__avatar">
          <img src={userAvatar} alt="post image0" />

          <h3>{userName}</h3>
        </div>
      </div>
      <div class="post_image">
        {/* //1 anh */}
        <img src={postImages[0]} alt="post image0" />
        {/* // nhieu anh */}
      </div>
      <div class="post__comments">
        {postComments.map((comment, index) => {
          return (
            <div key={index} class="comment">
              <h3>{comment.from}</h3>
              <p>{comment.text}</p>
            </div>
          );
        })}

        <div class="comment">
          <h3>Username</h3>
          <p>anh dep the</p>
        </div>
        <div class="comment">
          <h3>Username</h3>
          <p>anh dep the</p>
        </div>
        <div class="comment">
          <h3>Username</h3>
          <p>anh dep the</p>
        </div>
      </div>
    </div>
  );
}

export default Post;
