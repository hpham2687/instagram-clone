import React, { useState } from "react";

const UploadPost = (props) => {
  const { handleUploadPostUser } = props;
  const [file, setFile] = useState(null);

  const onChange = (e) => {
    setFile(e.target.files[0]);
    //console.log();
    //setFile
  };
  return (
    <>
      <progress value={40} max={100} />
      <input onChange={onChange} type="file"></input>
      <button onClick={() => handleUploadPostUser(file)}>Submit</button>
    </>
  );
};

export default UploadPost;
