import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import { Button, Input, Typography, useTheme } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import SwipeableViews from "react-swipeable-views";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 300,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  paper: {},
}));

const ModalAuth = (props) => {
  const classes = useStyles();
  const {
    open,
    handleOpen,
    handleClose,
    handleClickLogin,
    handleClickSignUp,
  } = props;
  const [formDataSignUp, setFormDataSignUp] = useState({
    username: "",
    email: "",
    password: "",
  });
  const [formDataLogin, setFormDataLogin] = useState({
    email: "",
    password: "",
  });

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const theme = useTheme();

  function a11yProps(index) {
    return {
      id: `full-width-tab-${index}`,
      "aria-controls": `full-width-tabpanel-${index}`,
    };
  }

  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && <Box p={3}>{children}</Box>}
      </div>
    );
  }

  const SignUpTab = (
    <div>
      <div className="modal__signup">
        <Input
          name="usernames"
          id="usernames"
          type="text"
          placeholder="krisspham"
          fullWidth
          onChange={(e) =>
            setFormDataSignUp((prevState) => ({
              ...prevState,
              username: e.target.value,
            }))
          }
          value={formDataSignUp.userName}
        ></Input>

        <Input
          name="emails"
          id="emails"
          type="email"
          placeholder="hpham2687@gmail.com"
          fullWidth
          onChange={(e) =>
            setFormDataSignUp((prevState) => ({
              ...prevState,
              email: e.target.value,
            }))
          }
          value={formDataSignUp.email}
        ></Input>
        <Input
          name="password"
          type="password"
          placeholder="krisspham"
          fullWidth
          onChange={(e) =>
            setFormDataSignUp((prevState) => ({
              ...prevState,
              password: e.target.value,
            }))
          }
          value={formDataSignUp.password}
        ></Input>
        <Button onClick={() => handleClickSignUp(formDataSignUp)}>
          Sign Up
        </Button>
      </div>
    </div>
  );

  const LoginTab = () => <div></div>;

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.root}>
            <AppBar position="static" color="default">
              <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                aria-label="full width tabs example"
              >
                <Tab label="Login" {...a11yProps(0)} />
                <Tab label="Register" {...a11yProps(1)} />
              </Tabs>
            </AppBar>

            {value === 1 ? (
              SignUpTab
            ) : (
              <div className="modal__signup">
                <Input
                  type="email"
                  placeholder="hpham2687@gmail.com"
                  fullWidth
                  onChange={(e) =>
                    setFormDataLogin((prevState) => ({
                      ...prevState,
                      email: e.target.value,
                    }))
                  }
                  defaultValue={formDataLogin.email}
                ></Input>
                <Input
                  type="password"
                  placeholder="krisspham"
                  fullWidth
                  onChange={(e) =>
                    setFormDataLogin((prevState) => ({
                      ...prevState,
                      password: e.target.value,
                    }))
                  }
                  defaultValue={formDataLogin.password}
                ></Input>
                <Button onClick={() => handleClickLogin(formDataLogin)}>
                  Sign Up
                </Button>
              </div>
            )}
          </div>
        </Fade>
      </Modal>
    </>
  );
};

export default ModalAuth;
