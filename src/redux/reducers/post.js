import {
  GET_USER_POST_REQUEST,
  GET_USER_POST_SUCCESS,
  GET_USER_POST_FAILURE,
} from "./../actions/constants";
const initialState = {
  isFetching: false,
  data: [],
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_USER_POST_REQUEST: {
      return { ...state, isFetching: true };
    }
    case GET_USER_POST_SUCCESS: {
      return { ...state, data: action.payload, isFetching: false, error: null };
    }
    case GET_USER_POST_FAILURE: {
      return { ...state, error: action.payload };
    }
    default:
      return state;
  }
}
