import { SET_AUTH_MODAL_STATUS } from "./../actions/constants";
const initialState = {
  modal: {
    isShowModalAuth: false,
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_AUTH_MODAL_STATUS: {
      return { ...state, modal: { isShowModalAuth: action.payload } };
    }

    default:
      return state;
  }
}
