import { combineReducers } from "redux";
import postReducer from "./../reducers/post";
import uiReducer from "./../reducers/ui";
import userReducer from "./../reducers/user";
const rootReducer = combineReducers({
  post: postReducer,
  ui: uiReducer,
  user: userReducer,
});

export default rootReducer;
