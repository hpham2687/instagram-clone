import { SET_USER_DATA } from "./../actions/constants";
const initialState = null;

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_USER_DATA: {
      return action.payload;
    }

    default:
      return state;
  }
}
