import { SET_USER_DATA } from "./../actions/constants";
export function setUserData(payload) {
  return {
    type: SET_USER_DATA,
    payload,
  };
}
