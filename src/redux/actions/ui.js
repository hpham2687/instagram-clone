import { SET_AUTH_MODAL_STATUS } from "./../actions/constants";
export function setModalAuthStatus(payload) {
  return {
    type: SET_AUTH_MODAL_STATUS,
    payload,
  };
}
