import firebaseCenter from "./../../firebase/config";
import {
  GET_USER_POST_REQUEST,
  GET_USER_POST_SUCCESS,
  GET_USER_POST_FAILURE,
} from "./../actions/constants";
export async function getUserData(dispatch, getState) {
  dispatch({ type: GET_USER_POST_REQUEST });
  try {
    const data = await firebaseCenter.userGetAllPost();
    if (data) dispatch({ type: GET_USER_POST_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: GET_USER_POST_FAILURE, payload: error });
  }
}
