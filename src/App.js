import logo from "./assets/img/Instagram_logo.png";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getUserData } from "./redux/actions/post";
import { setUserData } from "./redux/actions/user";
import firebaseCenter from "./firebase/config";

import HomeContainer from "./containers/home";
import ModalContainer from "./containers/modal";
import { useSnackbar } from "notistack";

import { setModalAuthStatus } from "./redux/actions/ui";

function App() {
  const user = useSelector((state) => state.user);
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserData);
  }, []);

  useEffect(() => {
    const unsubscribe = async () =>
      await firebaseCenter.auth.onAuthStateChanged(function (user) {
        if (user) {
          // User is signed in.
          console.log(user);
          dispatch(setUserData(user));

          // ...
        } else {
          dispatch(setUserData(null));
        }
      });
    unsubscribe();
    return () => unsubscribe();
  }, []);

  return (
    <div className="App">
      <header className="App__header">
        <div class="App__logo">
          <img src={logo} alt="logo img" />
        </div>
        <div class="App__auth">
          {user ? (
            <>
              {" "}
              <button
                onClick={async () => {
                  await firebaseCenter.auth.signOut();
                  enqueueSnackbar("Sign out successfully");
                }}
              >
                Sign Out
              </button>
            </>
          ) : (
            <>
              {" "}
              <button onClick={() => dispatch(setModalAuthStatus(true))}>
                Login
              </button>
            </>
          )}
        </div>
      </header>
      <HomeContainer />
      <ModalContainer />
    </div>
  );
}

export default App;
